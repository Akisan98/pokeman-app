import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";

import {StartPage} from "./pages/start/start.page";
import {TrainerPage} from "./pages/trainer/trainer.page";
import {CataloguePage} from "./pages/catalogue/catalogue.page";
import {AuthGuard} from "./auth.guard";

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/login'
  },
  {
    path: 'login',
    component: StartPage
  },
  {
    path: 'catalogue',
    component: CataloguePage,
    canActivate: [ AuthGuard ]
  },
  {
    path: 'trainer',
    component: TrainerPage,
    canActivate: [ AuthGuard ]
  },
  { path: '**', redirectTo: '/login' }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})

export class AppRoutingModule {}
