export interface Poke_Response {
  count: number;
  next: string;
  previous: string;
  results: Pokemon[];
}

export interface Pokemon {
    id: number,
    name: string,
    url: string,
    avatar: string
}

