import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {TrainersService} from "../../services/trainers.service";
import {Trainer} from "../../models/trainer.model";
import {SessionService} from "../../services/session.service";
import {NgForm} from "@angular/forms";

@Component({
  selector: 'app-start.page',
  templateUrl: './start.page.html',
  styleUrls: ['./start.page.css']
})
export class StartPage implements OnInit {
  trainer: Trainer | undefined;

  constructor(
    private router: Router,
    private readonly trainerService: TrainersService,
    private readonly sessionService: SessionService
  ) { }

  ngOnInit(): void {
    this.trainer = this.sessionService.trainer
    if (this.trainer !== undefined) {
      this.router.navigate(['catalogue']);
    }
  }

  get attempting(): boolean {
    return this.trainerService.attempting;
  }

  onSubmit(loginForm: NgForm): void {
    const { username } = loginForm.value
    this.trainerService.authenticate(username, async () => {
      await this.router.navigate(['catalogue'])
    })
  }
}
