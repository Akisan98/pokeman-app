import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {SessionService} from "../../services/session.service";
import {Trainer} from "../../models/trainer.model";
import {Pokemon} from "../../models/pokemon.model";
import {TrainersService} from "../../services/trainers.service";

@Component({
  selector: 'app-trainer',
  templateUrl: './trainer.page.html',
  styleUrls: ['./trainer.page.css']
})
export class TrainerPage implements OnInit {
  trainer: Trainer | undefined;

  constructor(
    private router: Router,
    private readonly sessionService: SessionService,
    private readonly trainerService: TrainersService
  ) { }

  ngOnInit(): void {
    this.trainer = this.sessionService.trainer
    //this.shouldBounce(this.name)
  }

  removePokemon(pokemon: Pokemon) {

    if (this.trainer !== undefined) {
      const remove = this.trainer.pokemon.indexOf(pokemon);
      this.trainer.pokemon.splice(remove, 1)
      this.sessionService.setUser(this.trainer);
      this.trainerService.updateTrainer(this.trainer)
    }
  }
}
