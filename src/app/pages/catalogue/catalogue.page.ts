import { Component, OnInit } from '@angular/core';
import { PokemonService } from "../../services/pokemon.service";
import {Pokemon} from "../../models/pokemon.model";
import {environment} from "../../../environments/environment";
import {Trainer} from "../../models/trainer.model";
import {SessionService} from "../../services/session.service";
import {TrainersService} from "../../services/trainers.service";

@Component({
  selector: 'app-catalogue',
  templateUrl: './catalogue.page.html',
  styleUrls: ['./catalogue.page.css']
})
export class CataloguePage implements OnInit {

  IMAGE_URL: string = environment.pokemonImageUrl

  pokemons: Pokemon[] = [];
  trainer: Trainer | undefined;
      
  constructor(
    private readonly pokemonService: PokemonService,
    private readonly sessionService: SessionService,
    private readonly trainerService: TrainersService
  ) { }

  ngOnInit(): void {
    if (this.pokemon.length === 0) {
      // Stop Fetch All the Time
      this.pokemonService.initRequest(
        () => {
          this.pokemons = this.pokemon;
          console.log(this.pokemons)
        }
      );
    } else {
      this.pokemons = this.pokemon;
      console.log("Have Pokes")
    }

    this.trainer = this.sessionService.trainer
  }

  collectPokemon(pokemon: Pokemon) {
    if (this.trainer === undefined) {
      alert("Something has gone wrong! Please Reload the App!")
    } else {
      if (this.trainer.pokemon === undefined) {
        this.trainer.pokemon = [];
      }

      if (this.pokeExists(pokemon)) {
        alert("Already Have This Pokemon")
      }      
      this.trainer.pokemon.push(pokemon);
      this.sessionService.setUser(this.trainer);
      this.trainerService.updateTrainer(this.trainer)
    }
  }

  loadNewPage() {
    this.pokemonService.pageRequest(
      () => {
        this.pokemons = this.pokemon;
        console.log(this.pokemons)
      }
    )
  }

  get pokemon(): Pokemon[] {
    return this.pokemonService.pokemon()
  }

  get isLoading(): boolean {
    return this.pokemonService.isLoading;
  }

  get isLoadingPage(): boolean {
    return this.pokemonService.isLoadingPage;
  }

  pokeExists(pokemon: Pokemon): boolean {
    for (const poke of this.trainer!.pokemon) {
      if (pokemon.id == poke.id) {
        
        return true;
      }
    }
    return false;
  }
}
