import {environment} from "../../environments/environment";
import {Poke_Response, Pokemon} from "../models/pokemon.model";
import {Injectable} from "@angular/core";
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {finalize} from "rxjs/operators";

const API_URL = environment.pokeUrl
const IMAGE_URL = environment.pokemonImageUrl

@Injectable({
    providedIn: 'root'
})
export class PokemonService {
    private _pokemon: Pokemon[] = [];
    private _error: string = '';
    public isLoading: boolean = false;
    public isLoadingPage: boolean = false;
    private _nextUrl: string = API_URL;

    constructor(private readonly http: HttpClient) {
    }

    public initRequest(onSuccess: () => void) {
      this.fetchPokemon(onSuccess, API_URL, this.isLoading);
    }

    public pageRequest(onSuccess: () => void) {
      this.fetchPokemon(onSuccess, this._nextUrl, this.isLoadingPage);
    }

    public fetchPokemon(onSuccess: () => void, url: string = API_URL, loading: boolean): void {
        console.log("Fetching More Pokemons");
        loading = true;
        this.http.get<Poke_Response>(url)
          .pipe(
            finalize( () => {
              loading = false;
            })
          )
          .subscribe((response: Poke_Response) => {
              const { next, results } = response;
              this._pokemon.push(...results);
              this._nextUrl = next;

              for (let i = 0; i < this._pokemon.length; i++) {
                this._pokemon[i].id = parseInt(this._pokemon[i].url.slice(34))
                this._pokemon[i].avatar = `${(IMAGE_URL)}${this._pokemon[i].id}.png`
              }

              onSuccess()
          }, (error: HttpErrorResponse) => {
              this._error = error.message;
          })
    }

    public pokemon(): Pokemon[] {
      return this._pokemon;
    }

    public error(): string {
      return this._error;
    }
}
