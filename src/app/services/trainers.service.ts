import { Injectable } from "@angular/core";
import {HttpClient, HttpErrorResponse, HttpHeaders} from "@angular/common/http";
import {Trainer} from "../models/trainer.model";
import {Observable, of} from "rxjs";
import {finalize, map, retry, switchMap, tap} from "rxjs/operators";
import {environment} from "../../environments/environment";
import {SessionService} from "./session.service";

const API_URL = environment.apiUrl

@Injectable({
  providedIn: 'root' // Make as Singleton
})
export class TrainersService {
  private _error : string = '';
  public attempting: boolean = false;

  // DI - Dependency Injection
  constructor(private readonly http: HttpClient, private sessionService: SessionService) {
  }

  updateTrainer(trainer: Trainer) {
    this.http.patch(
      `${API_URL}/trainers/${trainer.id}`,
      { pokemon: trainer.pokemon },
      {
        headers: new HttpHeaders({
          'X-API-Key': 'biq?BnhbCE&h3A8dpTz8p6!?NAf&HtEx3yE3TnosGcoH&hRKPDRPDhBmn!J$5Y79'
        })
      }
    ).subscribe()

  }

  private findByUserName(username: string): Observable<Trainer[]> {
    return this.http.get<Trainer[]>(`${API_URL}/trainers?username=${username}`)
  }

  private createUser(username: string): Observable<Trainer> {
    return this.http.post<Trainer>(
      `${API_URL}/trainers`,
      { username, pokemon: [] },
      {
        headers: new HttpHeaders({
          'X-API-Key': 'biq?BnhbCE&h3A8dpTz8p6!?NAf&HtEx3yE3TnosGcoH&hRKPDRPDhBmn!J$5Y79'
        })
      }
    )
  }

  public authenticate(username: string, onSucess: () => void) : void {
    this.attempting = true;

    this.findByUserName(username)
      .pipe(
        retry(3),
        switchMap( (trainer: Trainer[]) => {
          if (trainer.length) {
            return of(trainer[0])
          }
          return this.createUser(username)
        }),
        tap((trainer) => {
          this.sessionService.setUser(trainer);
        }),
        finalize(() => {
          this.attempting = false;
        })
      )
      .subscribe(
        (trainer: Trainer) => { // Success
          if (trainer.id) {
            onSucess()
          }
        },
        (error: HttpErrorResponse) => { // Error
          this._error = error.message
        }
      )
  }
}
