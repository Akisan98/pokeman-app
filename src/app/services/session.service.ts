import {Injectable} from "@angular/core";
import {Trainer} from "../models/trainer.model";

@Injectable({
  providedIn: 'root'
})
export class SessionService {
  private _trainer: Trainer | undefined;

  constructor() {
    const storedTrainer = localStorage.getItem('trainer')
    if (storedTrainer) {
      this._trainer = JSON.parse(storedTrainer) as Trainer;
    }
  }

  get trainer(): Trainer | undefined {
    return this._trainer;
  }

  setUser(trainer: Trainer) {
    this._trainer = trainer;
    localStorage.setItem('trainer', JSON.stringify(trainer))
  }

  logout() {
    this._trainer = undefined;
    localStorage.removeItem('trainer')
  }
}
