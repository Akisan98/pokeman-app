import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { SessionService } from "./services/session.service";
import {Trainer} from "./models/trainer.model";

@Component({
  selector: 'app-root',
  template: `
    <nav class="nav">
      <ul class="navbar">
        <li class="nav-item" routerLink="/login" *ngIf="trainer === undefined"><a class="nav-link">Home</a></li>
        <li class="nav-item" routerLink="/catalogue"><a class="nav-link" >Catalogue</a></li>
        <li class="nav-item" routerLink="/trainer"><a class="nav-link">Trainer</a></li>
      </ul>
      <button class="logout" (click)="removeName()">Logout</button>
    </nav>
    <router-outlet></router-outlet>`,
  styleUrls: ['./app.component.css']

})
export class AppComponent {

  constructor(
    private router: Router,
    private readonly sessionService: SessionService,
  ) { }

  removeName() {
    this.sessionService.logout();
    this.router.navigate(['login']);
  }

  get trainer(): Trainer | undefined {
    return this.sessionService.trainer
  }
}
