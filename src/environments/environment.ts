// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  apiUrl: "https://noroff-assignment-5-api.herokuapp.com",
  pokeUrl: "https://pokeapi.co/api/v2/pokemon?limit=20",
  pokemonImageUrl: 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/',
  apiKey: "biq?BnhbCE&h3A8dpTz8p6!?NAf&HtEx3yE3TnosGcoH&hRKPDRPDhBmn!J$5Y79"
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
