# Assignment 4 - Pokémon App

We have build an online Pokedex as a Single Page Application using Angular Framework. Where you can View, Collect and Explore Pokémon's

![Image of Pokedex](https://gitlab.com/Akisan98/pokeman-app/-/raw/main/src/assets/appImage.png)

### **Install:**

- npm
- Node.js
- Angular CLI
- Heroku CLI (For Pushing Project To Heroku For Hosting)

<br><br>

## Project setup

```plaintext
npm install

Dev:
ng serve

Build:
ng build
```

### <br><br>Component Tree

https://www.figma.com/file/6XYbipf17xSjHvqFyIpHxo/Assignment-7?node-id=0%3A1

### Trainers API Server

https://noroff-assignment-5-api.herokuapp.com/trainers

### Pokémon API Server

https://pokeapi.co/api/v2/pokemon?limit=20

### Live Solution

https://young-forest-16786.herokuapp.com/catalogue

<br><br>



## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.



